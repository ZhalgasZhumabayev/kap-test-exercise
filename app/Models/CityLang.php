<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CityLang extends Model
{
    use HasFactory;

    public $table = 'city_lang';

    protected $fillable = [
        'city_id',
        'title',
        'lang'
    ];

    protected $casts = [
        'city_id' => 'integer',
        'title' => 'string',
        'lang' => 'string',
        'create_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
