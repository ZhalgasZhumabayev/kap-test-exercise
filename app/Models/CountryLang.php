<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CountryLang extends Model
{
    use HasFactory;

    public $table = 'country_lang';

    protected $fillable = [
        'country_id',
        'title',
        'lang'
    ];

    protected $casts = [
        'country_id' => 'integer',
        'title' => 'string',
        'lang' => 'string',
        'create_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    public function countries()
    {
        return $this->hasMany(Country::class);
    }
}
