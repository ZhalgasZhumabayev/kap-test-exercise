<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'country_id'
    ];

    protected $casts = [
        'title' => 'string',
        'country_id' => 'integer',
        'create_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function cityLang()
    {
        return $this->hasMany(CityLang::class);
    }
}
