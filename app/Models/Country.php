<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    protected $fillable = [
        'title'
    ];

    protected $casts = [
        'title' => 'string',
        'create_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    public function cities()
    {
        return $this->hasMany(City::class);
    }

    public function countryLangs()
    {
        return $this->hasMany(CountryLang::class);
    }
}
