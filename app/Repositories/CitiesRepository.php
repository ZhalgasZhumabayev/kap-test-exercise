<?php


namespace App\Repositories;

use App\Models\City;
use App\Models\CityLang;

class CitiesRepository
{
    public function store($params){
        return City::create($params);
    }

    public function storeLangs($params){
        return CityLang::create($params);
    }

    public function show($id){
        return City::findOrFail($id);
    }

    public function update($params, $id){
        $model = City::findOrFail($id);

        if ($model && $params['title'] != null){
            unset($params['other_langs']);
            $model->update($params);
        }

        return $model;
    }

    public function updateLang($params){
        $model = CityLang::where(['city_id' => $params['city_id'], 'lang' => $params['lang']])->first();

        if ($model && $params['title'] != null){
            unset($params['city_id']);
            unset($params['lang']);
            $model->update($params);
        }

        return $model;
    }

    public function destroy($id){
        $model = City::findOrFail($id);

        if ($model){
            $langs = CityLang::where('city_id', $id)->get();

            foreach($langs as $k => $v){
                $v->delete();
            }

            $model->delete();
        }
    }
}
