<?php


namespace App\Repositories;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class BaseRepository
{
    public function paginateCollection($items, $perPage, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?? 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function index($params, $query)
    {
        return $this->prepareQuery($params, $query)->get();
    }

    public function indexPaginate($params, $query)
    {
        $perPage = $params['per_page'] ?? 5;
        return $this->prepareQuery($params, $query)->paginate($perPage);
    }

    private function prepareQuery($params, $query)
    {
        $query = $this->queryApplyFilter($query, $params);
        $query = $this->queryApplyOrderBy($query, $params);
        return $query;
    }

    private function queryApplyFilter($query, $params)
    {
        if (isset($params['q'])) {
            $query->where(function ($subQuery) use ($params) {
                $subQuery->where('title', 'like', '%' . $params['q'] . '%');
            });
        }

        return $query;
    }

    private function queryApplyOrderBy($query, $params)
    {
        $order = 'asc';
        if (isset($params['order']) && $params['order'] == 'desc') {
            $order = 'desc';
        }

        if (isset($params['sort'])) {
            $query->orderBy($params['sort'], $order);
        }
        return $query;
    }
}
