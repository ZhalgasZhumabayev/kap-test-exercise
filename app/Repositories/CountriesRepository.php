<?php


namespace App\Repositories;

use App\Models\Country;
use App\Models\CountryLang;

class CountriesRepository
{
    public function store($params){
        return Country::create($params);
    }

    public function storeLangs($params){
        return CountryLang::create($params);
    }

    public function show($id){
        return Country::findOrFail($id);
    }

    public function update($params, $id){
        $model = Country::findOrFail($id);

        if ($model && $params['title'] != null){
            unset($params['other_langs']);
            $model->update($params);
        }

        return $model;
    }

    public function updateLang($params){
        $model = CountryLang::where(['country_id' => $params['country_id'], 'lang' => $params['lang']])->first();

        if ($model && $params['title'] != null){
            unset($params['country_id']);
            unset($params['lang']);
            $model->update($params);
        }

        return $model;
    }

    public function destroy($id){
        $model = Country::findOrFail($id);

        if ($model){
            $langs = CountryLang::where('country_id', $id)->get();

            foreach($langs as $k => $v){
                $v->delete();
            }

            $model->delete();
        }
    }
}
