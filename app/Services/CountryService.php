<?php


namespace App\Services;

use App\Models\Country;
use App\Repositories\BaseRepository;
use App\Repositories\CountriesRepository;
use Illuminate\Support\Facades\Log;

class CountryService extends BaseService
{
    private $baseRepository;
    private $countriesRepository;

    public function __construct(BaseRepository $baseRepository, CountriesRepository $countriesRepository){
        $this->baseRepository = $baseRepository;
        $this->countriesRepository = $countriesRepository;
    }

    public function indexPaginate($params)
    {
        return $this->result($this->baseRepository->indexPaginate($params, Country::select('*')));
    }

    public function store($params){
        $result = $this->countriesRepository->store($params);

        if (isset($params['other_langs'])){
            foreach ($params['other_langs'] as $k => $v){
                $data = [
                    'country_id' => $result->id,
                    'title' => $v['title'],
                    'lang' => $v['lang']
                ];

                $this->countriesRepository->storeLangs($data);
            }
        }

        return $this->result($result);
    }

    public function show($id){
        return $this->result($this->countriesRepository->show($id));
    }

    public function update($params, $id){
        $result = $this->countriesRepository->update($params, $id);

        if (isset($params['other_langs'])){
            foreach ($params['other_langs'] as $k => $v){
                $data = [
                    'country_id' => $result->id,
                    'title' => $v['title'],
                    'lang' => $v['lang']
                ];

                $this->countriesRepository->updateLang($data);
            }
        }

        return $this->result($result);
    }

    public function destroy($id){
        $this->countriesRepository->destroy($id);
        return $this->ok();
    }
}
