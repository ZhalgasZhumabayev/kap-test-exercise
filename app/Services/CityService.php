<?php


namespace App\Services;

use App\Models\City;
use App\Repositories\BaseRepository;
use App\Repositories\CitiesRepository;
use Illuminate\Support\Facades\Log;

class CityService extends BaseService
{
    private $baseRepository;
    private $citiesRepository;

    public function __construct(BaseRepository $baseRepository, CitiesRepository $citiesRepository){
        $this->baseRepository = $baseRepository;
        $this->citiesRepository = $citiesRepository;
    }

    public function indexPaginate($params)
    {
        return $this->result($this->baseRepository->indexPaginate($params, City::select('*')));
    }

    public function store($params){
        $result = $this->citiesRepository->store($params);

        if (isset($params['other_langs'])){
            foreach ($params['other_langs'] as $k => $v){
                $data = [
                    'city_id' => $result->id,
                    'title' => $v['title'],
                    'lang' => $v['lang']
                ];

                $this->citiesRepository->storeLangs($data);
            }
        }

        return $this->result($result);
    }

    public function show($id){
        return $this->result($this->citiesRepository->show($id));
    }

    public function update($params, $id){
        $result = $this->citiesRepository->update($params, $id);

        if (isset($params['other_langs'])){
            foreach ($params['other_langs'] as $k => $v){
                $data = [
                    'city_id' => $result->id,
                    'title' => $v['title'],
                    'lang' => $v['lang']
                ];

                $this->citiesRepository->updateLang($data);
            }
        }

        return $this->result($result);
    }

    public function destroy($id){
        $this->citiesRepository->destroy($id);
        return $this->ok();
    }
}
