<?php


namespace App\Services;

class BaseService
{
    protected function errValidate($message)
    {
        return $this->error(422, $message);
    }

    protected function errFobidden($message)
    {
        return $this->error(403, $message);
    }

    protected function errNotFound($message)
    {
        return $this->error(404, $message);
    }

    protected function errService($message)
    {
        return $this->error(500, $message);
    }

    protected function notAcceptable($message)
    {
        return $this->error(406, $message);
    }

    protected function ok($message = 'OK')
    {
        return $this->result([
            'message' => $message,
        ]);
    }

    protected function result($data)
    {
        return new ServiceResult([
            'data' => $data,
            'code' => 200
        ]);
    }

    protected function error(int $code, string $message)
    {
        return new ServiceResult([
            'data' => ['message' => $message],
            'code' => $code
        ]);
    }
}
