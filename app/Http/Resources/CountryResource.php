<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CountryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'langs' => $this->when($this->countryLangs->count() > 0, new CountryLangCollection($this->countryLangs)),
            'cities' => $this->when($this->cities->count() > 0, new CityCollection($this->cities)),
        ];
    }
}
