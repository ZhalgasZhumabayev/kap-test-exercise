<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Country;
use App\Models\CountryLang;
use App\Models\City;
use App\Models\CityLang;
use Illuminate\Database\Eloquent\Factories\Sequence;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        Country::factory(10)
            ->has(CountryLang::factory()->count(2)->state(new Sequence(['lang' => 'en'], ['lang' => 'kk'])))
            ->has(City::factory()->count(3)->has(CityLang::factory()->count(2)->state(new Sequence(['lang' => 'en'], ['lang' => 'kk']))))
            ->create();
    }
}
